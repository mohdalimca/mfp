import 'package:get_storage/get_storage.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';

class AppUtils {
  static saveUser(User user) {
    GetStorage box = GetStorage();
    Map<String,dynamic> userJson = user.toJson();
    box.write(PrefKeys.user, userJson);
  }

  static Future<User> getUser() async{
    GetStorage box = GetStorage();
    Map<String, dynamic> userJson = await box.read(PrefKeys.user);
    if (userJson == null) return null;
    return User.fromJson(userJson);
  }

  static logout() {
    GetStorage box = GetStorage();
    box.remove(PrefKeys.user);
  }
}

class PrefKeys {
  static String user = 'user';
}
