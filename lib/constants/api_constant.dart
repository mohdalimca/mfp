final String baseUrl = "http://knowledgerevise.com/";
final String apiVersion = "MFP/webservices/";

class Apis {
  static String registration = baseUrl + apiVersion + "signup";
  static String login = baseUrl + apiVersion + "users/login";
  static String forgotPassword = baseUrl + apiVersion + "users/forgotpassword";
  static String changePassword = baseUrl + apiVersion + "users/changepassword";
  static String aboutUs = baseUrl + apiVersion + "static_page/about_app";
  static String privacyPolicy = baseUrl + apiVersion + "static_page/privacy_policy";
  static String termsAndCondition = baseUrl + apiVersion + "static_page/terms_condition";
  static String getProfile = baseUrl + apiVersion + "users/get_profile";
  static String logout = baseUrl + apiVersion + "logout";
  static String homeManager = baseUrl + apiVersion + "users/manager_dashboard";
}


class AppVersion{
  static dynamic iOS = "1.0.0";
  static dynamic android = "1.0.0";
  static dynamic version = "1.0.0";
}
