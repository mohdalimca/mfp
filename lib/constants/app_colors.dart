
 import 'package:flutter/material.dart';

class AppColor{
   static Color whiteColor = Colors.white;
   static Color blackColor = Colors.black;
   static Color blackColor12 = Colors.black12;
   static Color blackColor54 = Colors.black54;
   static Color grey200Color = Colors.grey[200];
   static Color primaryOrangeColor = Color(0xFFF35B2D);
   static Color primaryYellowColor = Color(0xFFFAA33C);
   static Color whiteColor1 = Color(0xFFF7F7FA);
   static Color dotActiveColor = Color(0xFF0F0D36);
   static Color dotInActiveColor = Color(0xCC0F0D36);
   static Color pinkColor = Color(0xFF97268E);
}