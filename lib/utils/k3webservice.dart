import 'package:dio/dio.dart';
import 'package:my_feedback_pal/constants/api_constant.dart';
import 'package:my_feedback_pal/constants/string_constant.dart';
import 'package:my_feedback_pal/screens/home_tab/model.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';
import 'package:my_feedback_pal/screens/static_page_screen/model.dart';

class K3Webservice {
  static Future<ApiResponse<T>> postMethod<T>(
      String url, dynamic data, dynamic headers) async {
    Dio dio = new Dio();
    print('hitting url: ' + url);
    print('with parameter: ' + data.toString());
    print('with headers: ' + headers.toString());
    if (headers == null)
      headers = {'version': AppVersion.version};
    else
      headers['version'] = AppVersion.version;
    try {
      //var response = await http.post(url, body: data, headers: headers);
      var response =
          await dio.post(url, data: data, options: Options(headers: headers));
      print(response.data);
      if (response.data["message"] == "Auth failed" ||
          response.data["msg"] == "Auth failed") {
        return ApiResponse(
            error: true, message: StringConstant.sessionExpiredText);
      }
      if (response.statusCode == 200) {
        if (response.data["status"] == 0) {
          return ApiResponse<T>(error: true, message: response.data["message"]);
        }
        return ApiResponse<T>(data: fromJson<T>(response.data));
      } else if (response.statusCode == 422) {
        return ApiResponse<T>(error: true, message: "Something went wrong");
      } else {
        return ApiResponse<T>(error: true, message: "Something went wrong");
      }
    } on DioError catch (e) {
      print(e);
      print(e.response);
      if (e.response == null)
        return ApiResponse<T>(error: true, message: "Something went wrong");
      return ApiResponse<T>(
            error: true, message: e.response.data['message']);
    }
  }

  static Future<ApiResponse<T>> getMethod<T>(
      String url, dynamic headers) async {
    Dio dio = new Dio();

     if (headers == null)
      headers = {'version': AppVersion.version};
    else
      headers['version'] = AppVersion.version;
    print('hitting url: ' + url);
    print('with headers: ' + headers.toString());

    //var response = await http.get(url, headers: headers);

    try {
      var response = await dio.get(url, options: Options(headers: headers));
      print(response.data);
      if (response.data["message"] == "Auth failed" ||
          response.data["msg"] == "Auth failed") {
        return ApiResponse(
            error: true, message: StringConstant.sessionExpiredText);
      }
      if (response.statusCode == 200) {
        if (response.data["status"] == 0) {
          return ApiResponse<T>(error: true, message: response.data["message"]);
        }
        return ApiResponse<T>(data: fromJson<T>(response.data));
      } else if (response.statusCode == 422) {
        return ApiResponse<T>(error: true, message: "Something went wrong");
      } else {
        return ApiResponse<T>(error: true, message: "Something went wrong");
      }
    } on DioError catch (e) {
      print(e);
      print(e.response);
      if (e.response == null)
        return ApiResponse<T>(error: true, message: "Something went wrong");
      return ApiResponse<T>(
            error: true, message: e.response.data['message']);
    }
  }

  static T fromJson<T>(dynamic json) {
    if (T == CommonResponseModel) {
      return CommonResponseModel.fromJson(json) as T;
    } else if (T == LoginResponseModel) {
      return LoginResponseModel.fromJson(json) as T;
    } else if (T == StaticPageResponseModel) {
      return StaticPageResponseModel.fromJson(json) as T;
    } else if (T == ManagerHomeResponseModel) {
      return ManagerHomeResponseModel.fromJson(json) as T;
    }else {
      return null;
      //throw Exception("Unknown class");
    }
  }
}

class ApiResponse<T> {
  T data;
  String message;
  bool error;
  ApiResponse({this.data, this.error = false, this.message});
}

class CommonResponseModel {
  int status;
  String message;

  CommonResponseModel({
    this.status,
    this.message,
  });

  factory CommonResponseModel.fromJson(Map<String, dynamic> json) =>
      CommonResponseModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
      };
}
