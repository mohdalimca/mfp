import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:my_feedback_pal/constants/string_constant.dart';
import 'package:my_feedback_pal/screens/change_password_screen/change_password_screen.dart';
import 'package:my_feedback_pal/screens/forgot_passowrd_screen/forgot_password_screen.dart';
import 'package:my_feedback_pal/screens/home_screen.dart';
import 'package:my_feedback_pal/screens/onboarding_screen/onboarding_screen.dart';
import 'package:my_feedback_pal/screens/login_screen/login_screen.dart';
import 'package:my_feedback_pal/screens/profile_screen/profile_screen.dart';
import 'package:my_feedback_pal/screens/splash_screen.dart';
import 'package:my_feedback_pal/screens/static_page_screen/static_page_screen.dart';
import 'package:my_feedback_pal/screens/team_member_profile_screen/team_member_profile_screen.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: StringConstant.appName,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'SF UI Display'),
      home: SplashScreen(),
      onGenerateRoute: routes,
    );
  }

  Route routes(RouteSettings settings) {
    var page;

    switch (settings.name) {
      case SplashScreen.routeName:
        page = SplashScreen();
        break;
      case LoginScreen.routeName:
        page = LoginScreen();
        break;
      case ForgotPasswordScreen.routeName:
        page = ForgotPasswordScreen();
        break;
      case HomeScreen.routeName:
        page = HomeScreen();
        break;
      case ProfileScreen.routeName:
        page = ProfileScreen();
        break;
      case TeamMemberProfileScreen.routeName:
        page = TeamMemberProfileScreen();
        break;

      case StaticPageScreen.routeName:
        page = StaticPageScreen(
          staticPageType: settings.arguments,
        );
        break;
      case ChangePasswordScreen.routeName:
        page = ChangePasswordScreen();
        break;
      case OnboardingScreen.routeName:
        page = OnboardingScreen();
        break;
      default:
        return null;
    }

    return CupertinoPageRoute(builder: (context) => page);
  }
}
