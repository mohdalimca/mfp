
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';

class ProfileImageWidget extends StatelessWidget {
  final String url;
  final double size;

  const ProfileImageWidget({Key key, this.url, this.size}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size / 2),
          boxShadow: [
            BoxShadow(
                color: AppColor.blackColor54, blurRadius: 2, spreadRadius: 0.5)
          ],
          color: AppColor.primaryOrangeColor,
          image: DecorationImage(
              image: CachedNetworkImageProvider(url), fit: BoxFit.cover)),
    );
  }
}