import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';

showLoadingIndicator(BuildContext context) {
  AlertDialog alert = AlertDialog(
    backgroundColor: AppColor.primaryOrangeColor,
    content: new Row(
      children: [
        CupertinoActivityIndicator(),
        Container(margin: EdgeInsets.only(left: 15), child: Text("Loading",style: TextStyle(color:AppColor.whiteColor),)),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

hideLoadingIndicator(BuildContext context) {
  Navigator.pop(context);
}
