import 'package:flutter/material.dart';

class BackButtonWidget extends StatelessWidget {
  final Function onTap;
  const BackButtonWidget({
    Key key,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Image.asset(
            'assets/images/arrows@2x.png',
            height: 15,
          ),
        ),
      ),
    );
  }
}
