import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';

class ButtonWiget extends StatelessWidget {
  final Function onTap;
  final String title;
  const ButtonWiget({
    Key key,
    this.onTap,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            stops: [0.6, 1.0],
            colors: <Color>[Color(0xCCF35B2D), Color(0x80FAA33C)],
          ),
        ),
        child: Center(
            child: Text(
          title,
          style: TextStyle(color: AppColor.whiteColor, fontSize: 16),
        )),
      ),
    );
  }
}
