
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/widgets/back_button_widget.dart';
import 'package:my_feedback_pal/widgets/button_wiget.dart';

import 'forgot_password_manager.dart';

class ForgotPasswordScreen extends StatefulWidget {
  static const String routeName = '/forgotPassword';
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  ForgortPasswordManager _forgortPasswordManager = ForgortPasswordManager();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: BackButtonWidget(
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Forgot Password',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: AppColor.blackColor,
                          fontSize: 27,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20),
                    Text(
                      'Password reset link will be sent to your \nregistered email id',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: AppColor.blackColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w100),
                    ),
                    buildForm(),
                    SizedBox(height: 50),
                    ButtonWiget(
                        onTap: () {
                          if (_fbKey.currentState.saveAndValidate()) {
                            print(_fbKey.currentState.value);
                            FocusScope.of(context).unfocus();
                            _forgortPasswordManager.callForgotPasswordApi(context,_fbKey.currentState.value);
                          }
                        },
                        title: 'Submit')
                  ]),
            )
          ],
        )),
      ),
    );
  }

  Widget buildForm() {
    return Column(children: [
      FormBuilder(
        key: _fbKey,
        child: Column(children: [
          FormBuilderTextField(
            attribute: "email",
            maxLines: 1,
            decoration: InputDecoration(
                labelText: "Email",
                labelStyle: TextStyle(color: AppColor.blackColor)),
            keyboardType: TextInputType.emailAddress,
            validators: [
              FormBuilderValidators.email(),
              FormBuilderValidators.required()
            ],
          ),
        ]),
      )
    ]);
  }
}
