import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/screens/profile_screen/profile_screen_manager.dart';
import 'package:my_feedback_pal/screens/static_page_screen/model.dart';
import 'package:my_feedback_pal/screens/static_page_screen/static_page_screen.dart';
import 'package:my_feedback_pal/widgets/back_button_widget.dart';
import 'package:my_feedback_pal/widgets/profile_image_widget.dart';

import '../change_password_screen/change_password_screen.dart';
import '../login_screen/login_screen.dart';

class ProfileScreen extends StatefulWidget {
  static const String routeName = "/profileScreen";
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool _isLoading = false;
  ProfileScreenManager _profileScreenManager = ProfileScreenManager();
  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? Center(child: CupertinoActivityIndicator())
          : buildBody(context),
    );
  }

  SafeArea buildBody(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          buildAppBar(context),
          SizedBox(height: 20),
          buildUpperProfileRow(),
          SizedBox(height: 40),
          buildInfoRow('Phone', '7837362828'),
          SizedBox(height: 20),
          buildInfoRow('Email', _profileScreenManager.getEmail()),
          SizedBox(height: 20),
          buildInfoRow('Date of Birth', '05 Mar 1997'),
          SizedBox(height: 20),
          buildInfoRow('Manager', 'Teddy'),
          SizedBox(height: 40),
          Text(
            'Settings',
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          ),
          SizedBox(height: 20),
          buildSettingRow('change password', () {
            Navigator.pushNamed(context, ChangePasswordScreen.routeName);
          }),
          SizedBox(height: 20),
          buildSettingRow('Terms and condition', () {
            Navigator.pushNamed(context, StaticPageScreen.routeName,
                arguments: StaticPageType.TermsAndCondition);
          }),
          SizedBox(height: 20),
          buildSettingRow('Privacy Policy', () {
            Navigator.pushNamed(context, StaticPageScreen.routeName,
                arguments: StaticPageType.PrivacyPolicy);
          }),
          SizedBox(height: 20),
          buildSettingRow('Contact Us', () {}),
          SizedBox(height: 20),
          buildSettingRow('Log out', () {
            AppUtils.logout();
            Navigator.pushNamed(context, LoginScreen.routeName);
          }),
          SizedBox(height: 20),
        ]),
      ),
    ));
  }

  Row buildUpperProfileRow() {
    return Row(
      children: [
        ProfileImageWidget(size: 100,url: _profileScreenManager.getUserProfileUrl(),),
        SizedBox(width: 10),
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            _profileScreenManager.getUserName(),
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          Text(
            'Developer',
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
          ),
          SizedBox(height: 20),
          InkWell(onTap: () {}, child: Text('Edit Profile'))
        ])
      ],
    );
  }

  InkWell buildSettingRow(String title, Function onTap) {
    return InkWell(
      onTap: onTap,
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
            ),
            Icon(Icons.chevron_right)
          ],
        ),
        Divider()
      ]),
    );
  }

  Column buildInfoRow(String title, String detail) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        title,
        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
      ),
      SizedBox(height: 5),
      Text(
        detail,
        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
      ),
    ]);
  }

  Row buildAppBar(BuildContext context) {
    return Row(
      children: [
        BackButtonWidget(onTap: () {
          Navigator.pop(context);
        }),
        Text(
          'My Profile',
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
        )
      ],
    );
  }

  getUserInfo() async {
    setState(() {
      _isLoading = true;
    });
    await _profileScreenManager.getUser();
    setState(() {
      _isLoading = false;
    });
  }
}

