import 'package:get/state_manager.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';

class ProfileScreenManager extends GetxController{
  User _user;

  getUser() async {
    _user = await AppUtils.getUser();
  }

  String getUserName() {
    if (_user == null) return '';
    return (_user.firstname ?? '') + ' ' + (_user.lastname ?? '');
  }

  String getEmail() {
    if (_user == null) return '';
    return (_user.userEmail ?? '');
  }

  String getUserProfileUrl() {
    if (_user == null) return '';
    return (_user.userPic ?? '');
  }
}
