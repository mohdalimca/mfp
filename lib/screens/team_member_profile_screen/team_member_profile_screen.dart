import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/widgets/back_button_widget.dart';
import 'package:my_feedback_pal/widgets/profile_image_widget.dart';

class TeamMemberProfileScreen extends StatefulWidget {
  static const String routeName = '/teamMemberProfileScreen';
  @override
  _TeamMemberProfileScreenState createState() => _TeamMemberProfileScreenState();
}

class _TeamMemberProfileScreenState extends State<TeamMemberProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody(context),
    );
  }

  SafeArea buildBody(BuildContext context) {
    return SafeArea(child: Column(
      children:[
        buildAppBar(context),
        SizedBox(height:10),
        buildTopTimelineImageContainer(),
        Column(
          children:[
            buildUserInfoRow('Email','jamens@gmail.com'),
            buildUserInfoRow('Phone','726472424'),
            buildUserInfoRow('Date of Birth','05 Mar 2020'),
            buildUserInfoRow('Designation','Designer'),
          ]
        )
      ]
    ));
  }

  Widget buildUserInfoRow(String title,String description) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Text(title,style:TextStyle(fontWeight: FontWeight.w300)),
              Text(description,style:TextStyle(fontWeight: FontWeight.w300))
            ],),
    );
  }

  Container buildTopTimelineImageContainer() {
    return Container(
        height:200,
        child: Stack(
          children: [
            Container(
              height:80,
        decoration: BoxDecoration(
          color:AppColor.primaryOrangeColor,
          image:DecorationImage(
            image:AssetImage('assets/images/Image47@2x.png'),
            fit:BoxFit.cover
          )
        ),
            ),
            buildCircleCollection(),
            buildUpperProfileView()
          ],
        ),
      );
  }

  Align buildUpperProfileView() {
    return Align(
              alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                children:[
                  ProfileImageWidget(size: 120,url: 'http://knowledgerevise.com/MFP/uploads/user/default.png',),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children:[
                        SizedBox(height:35),
                        Text('Name User',textAlign: TextAlign.left,style:TextStyle(fontWeight: FontWeight.w600)),
                         FlatButton(
                                  onPressed: () {
                                    // Navigator.pushNamed(context, TeamMemberProfileScreen.routeName);
                                  },
                                  child: Text(
                                    'Submit review',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w200),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  color: AppColor.grey200Color,
                                  shape: StadiumBorder(),
                                )
                      ]
                    ),
                  )
                ]
              ),
                            ),
            );
  }

   Row buildAppBar(BuildContext context) {
    return Row(
      children: [
        BackButtonWidget(onTap: () {
          Navigator.pop(context);
        }),
        Text(
          'Team Member',
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
        )
      ],
    );
  }


  Positioned buildCircleCollection() {
    return Positioned(
      bottom: 25,
      right: -10,
      child: Column(
        children: [
          Row(
            children: [
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
             Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
             Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
             Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
             Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
             Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
             Opacity(
                opacity: 0.2,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.primaryOrangeColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

}