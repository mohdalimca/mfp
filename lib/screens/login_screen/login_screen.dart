import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/screens/login_screen/login_manager.dart';
import 'package:my_feedback_pal/widgets/button_wiget.dart';

import '../forgot_passowrd_screen/forgot_password_screen.dart';
import '../home_screen.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = '/loginScreen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  FocusNode emailFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  LoginManager _loginManager = LoginManager();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(children: [
          buildBgImageContainer(),
          Container(
            height: Get.height,
            width: Get.width,
            child: Column(children: [
              buildTopContainer(),
              buildBottomContainer(),
            ]),
          )
        ]),
      ),
    );
  }

  Container buildBottomContainer() {
    return Container(
      height: Get.height / 2,
      width: Get.width,
      decoration: BoxDecoration(
          color: AppColor.whiteColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50), topRight: Radius.circular(50))),
      child: SingleChildScrollView(
          child: Column(
        children: [
          SizedBox(height: 20),
          Image.asset(
            'assets/images/1b2197_9397be49502f4f6ba27c3f2d1a2cc9f1_mv2@2x.png',
            height: 50,
          ),
          SizedBox(height: 20),
          buildForm(),
          FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, ForgotPasswordScreen.routeName);
              },
              child: Text(
                'Forgot Password?',
                style: TextStyle(color: AppColor.primaryOrangeColor),
              )),
          SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: ButtonWiget(
              onTap: () {
                if (_fbKey.currentState.saveAndValidate()) {
                  print(_fbKey.currentState.value);
                  FocusScope.of(context).unfocus();
                  _loginManager.callLoginApi(context, _fbKey.currentState.value);
                }
              },
              title: 'Login',
            ),
          )
        ],
      )),
    );
  }

  Widget buildForm() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(children: [
        FormBuilder(
          key: _fbKey,
          child: Column(children: [
            FormBuilderTextField(
              attribute: "email",
              decoration: InputDecoration(
                labelText: "Email",
                labelStyle: TextStyle(color: AppColor.blackColor),
              ),
              keyboardType: TextInputType.emailAddress,
              focusNode: emailFocusNode,
              maxLines: 1,
              onFieldSubmitted: (text) {
                emailFocusNode.unfocus();
                passwordFocusNode.requestFocus();
              },
              validators: [
                FormBuilderValidators.email(),
                FormBuilderValidators.required()
              ],
            ),
            SizedBox(height: 10),
            FormBuilderTextField(
              attribute: "password",
              focusNode: passwordFocusNode,
              decoration: InputDecoration(
                  labelText: "Password",
                  labelStyle: TextStyle(color: AppColor.blackColor)),
              obscureText: true,
              maxLines: 1,
              onFieldSubmitted: (text) {
                passwordFocusNode.unfocus();
              },
              validators: [
                FormBuilderValidators.required(),
              ],
            ),
          ]),
        )
      ]),
    );
  }

  Container buildTopContainer() {
    return Container(
      height: Get.height / 2,
      width: Get.width,
      child: Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Image.asset(
                'assets/images/logo@2x.png',
                height: 50,
              ),
              SizedBox(height: 20),
              Text(
                'Welcome back',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              Text(
                'Login to your account',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                    fontWeight: FontWeight.w100),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
            ],
          )),
    );
  }

  Container buildBgImageContainer() {
    return Container(
      height: Get.height,
      width: Get.width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[Color(0xCCF35B2D), Color(0x80FAA33C)],
          ),
          image: DecorationImage(
            image: AssetImage('assets/images/Group1511@2x.png'),
            fit: BoxFit.cover,
          )),
    );
  }
}
