
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_feedback_pal/constants/api_constant.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/constants/string_constant.dart';
import 'package:my_feedback_pal/screens/home_screen.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';
import 'package:my_feedback_pal/utils/k3webservice.dart';
import 'package:my_feedback_pal/widgets/loading_indication.dart';


class LoginManager{
  callLoginApi(BuildContext context,Map<String, dynamic> values) async {
     showLoadingIndicator(context);
    ApiResponse<LoginResponseModel> apiResponse =
        await K3Webservice.postMethod(Apis.login, values, null);
        hideLoadingIndicator(context);
        if (apiResponse.error){
          Get.snackbar(StringConstant.appName, apiResponse.message,snackPosition:SnackPosition.BOTTOM,backgroundColor: AppColor.primaryOrangeColor,colorText: AppColor.whiteColor);
          return;
        }
        Get.snackbar(StringConstant.appName, apiResponse.data.message,snackPosition:SnackPosition.BOTTOM,backgroundColor: AppColor.primaryOrangeColor,colorText: AppColor.whiteColor);
        AppUtils.saveUser(apiResponse.data.data.login);
        await Future.delayed(Duration(seconds: 3));
        Navigator.pushReplacementNamed(context, HomeScreen.routeName);
  }
}