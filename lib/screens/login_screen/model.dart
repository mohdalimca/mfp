class LoginResponseModel {
    LoginResponseModel({
        this.version,
        this.status,
        this.errorcode,
        this.message,
        this.data,
    });

    Version version;
    int status;
    int errorcode;
    String message;
    Data data;

    factory LoginResponseModel.fromJson(Map<String, dynamic> json) => LoginResponseModel(
        version: json["version"] == null ? null : Version.fromJson(json["version"]),
        status: json["status"] == null ? null : json["status"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "version": version == null ? null : version.toJson(),
        "status": status == null ? null : status,
        "errorcode": errorcode == null ? null : errorcode,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class Data {
    Data({
        this.login,
    });

    User login;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        login: json["login"] == null ? null : User.fromJson(json["login"]),
    );

    Map<String, dynamic> toJson() => {
        "login": login == null ? null : login.toJson(),
    };
}

class User {
    User({
        this.authorization,
        this.userId,
        this.companyId,
        this.userEmail,
        this.firstname,
        this.lastname,
        this.designation,
        this.managerId,
        this.userRole,
        this.userPic,
        this.userStatus,
    });

    String authorization;
    String userId;
    String companyId;
    String userEmail;
    String firstname;
    String lastname;
    String designation;
    String managerId;
    String userRole;
    String userPic;
    int userStatus;

    factory User.fromJson(Map<String, dynamic> json) => User(
        authorization: json["Authorization"],
        userId: json["user_id"],
        companyId: json["company_id"],
        userEmail: json["user_email"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        designation: json["designation"],
        managerId: json["manager_id"],
        userRole: json["user_role"],
        userPic: json["user_pic"],
        userStatus: json["user_status"],
    );

    Map<String, dynamic> toJson() => {
        "Authorization": authorization,
        "user_id": userId,
        "company_id": companyId,
        "user_email": userEmail,
        "firstname": firstname,
        "lastname": lastname,
        "designation": designation,
        "manager_id": managerId,
        "user_role": userRole,
        "user_pic": userPic,
        "user_status": userStatus,
    };
}

class Version {
    Version({
        this.status,
        this.versioncode,
        this.versionmessage,
        this.currentVersion,
    });

    int status;
    int versioncode;
    String versionmessage;
    String currentVersion;

    factory Version.fromJson(Map<String, dynamic> json) => Version(
        status: json["status"] == null ? null : json["status"],
        versioncode: json["versioncode"] == null ? null : json["versioncode"],
        versionmessage: json["versionmessage"] == null ? null : json["versionmessage"],
        currentVersion: json["current_version"] == null ? null : json["current_version"],
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "versioncode": versioncode == null ? null : versioncode,
        "versionmessage": versionmessage == null ? null : versionmessage,
        "current_version": currentVersion == null ? null : currentVersion,
    };
}
