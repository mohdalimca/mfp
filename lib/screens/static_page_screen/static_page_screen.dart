import 'dart:async';

import 'package:flutter/material.dart';
import 'package:my_feedback_pal/screens/static_page_screen/model.dart';
import 'package:my_feedback_pal/screens/static_page_screen/static_page_manager.dart';
import 'package:my_feedback_pal/widgets/back_button_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

class StaticPageScreen extends StatefulWidget {
  static const String routeName = '/staticPageScreen';
  final StaticPageType staticPageType;

  const StaticPageScreen({Key key, @required this.staticPageType})
      : super(key: key);
  @override
  _StaticPageScreenState createState() => _StaticPageScreenState();
}

class _StaticPageScreenState extends State<StaticPageScreen> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  String urlToLoad = '';
  StaticPageManager _staticPageManager = StaticPageManager();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      callApi();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
        padding: const EdgeInsets.all(15.0),
        child: buildAppBar(context),
      ),
      urlToLoad == ''
          ? Container() 
          : Expanded(
              child: WebView(
                initialUrl:
                    urlToLoad,
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
                javascriptChannels: <JavascriptChannel>[
                  //_toasterJavascriptChannel(context),
                ].toSet(),
                navigationDelegate: (NavigationRequest request) {
                  if (request.url.startsWith('https://www.youtube.com/')) {
                    print('blocking navigation to $request}');
                    return NavigationDecision.prevent;
                  }
                  print('allowing navigation to $request');
                  return NavigationDecision.navigate;
                },
                onPageStarted: (String url) {
                  print('Page started loading: $url');
                },
                onPageFinished: (String url) {
                  print('Page finished loading: $url');
                },
                gestureNavigationEnabled: true,
              ),
            )
    ])));
  }

  Row buildAppBar(BuildContext context) {
    return Row(
      children: [
        BackButtonWidget(onTap: () {
          Navigator.pop(context);
        }),
        Text(
          getTitle(widget.staticPageType),
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
        )
      ],
    );
  }

  callApi() async {
    String url = await _staticPageManager.callStaticPageApi(
        context, getUrl(widget.staticPageType));
    setState(() {
      urlToLoad = url;
    });
  }
}
