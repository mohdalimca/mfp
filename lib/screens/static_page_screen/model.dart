import 'package:my_feedback_pal/constants/api_constant.dart';
import 'package:my_feedback_pal/screens/static_page_screen/static_page_screen.dart';

class StaticPageResponseModel {
  StaticPageResponseModel({
    this.status,
    this.errorcode,
    this.message,
    this.data,
  });

  int status;
  int errorcode;
  String message;
  Data data;

  factory StaticPageResponseModel.fromJson(Map<String, dynamic> json) =>
      StaticPageResponseModel(
        status: json["status"] == null ? null : json["status"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "errorcode": errorcode == null ? null : errorcode,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.url,
  });

  String url;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
      };
}

enum StaticPageType { PrivacyPolicy, TermsAndCondition, AboutUs }

String getTitle(StaticPageType staticPageType) {
  switch (staticPageType) {
    case StaticPageType.AboutUs:
      return 'About Us';
      break;
    case StaticPageType.TermsAndCondition:
      return 'Terms And Conditions';
      break;
    case StaticPageType.PrivacyPolicy:
      return 'Privacy Policy';
      break;
    default:
      return '';
  }
}

String getUrl(StaticPageType staticPageType) {
  switch (staticPageType) {
    case StaticPageType.AboutUs:
      return Apis.aboutUs;
      break;
    case StaticPageType.TermsAndCondition:
      return Apis.termsAndCondition;
      break;
    case StaticPageType.PrivacyPolicy:
      return Apis.privacyPolicy;
      break;
    default:
      return '';
  }
}
