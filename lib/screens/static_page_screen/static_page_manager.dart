
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_feedback_pal/constants/api_constant.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/constants/string_constant.dart';
import 'package:my_feedback_pal/screens/static_page_screen/model.dart';
import 'package:my_feedback_pal/utils/k3webservice.dart';
import 'package:my_feedback_pal/widgets/loading_indication.dart';


class StaticPageManager{
  Future<String> callStaticPageApi(BuildContext context,String url) async {
     showLoadingIndicator(context);
    ApiResponse<StaticPageResponseModel> apiResponse =
        await K3Webservice.getMethod(url, null);
        hideLoadingIndicator(context);
        if (apiResponse.error){
          Get.snackbar(StringConstant.appName, apiResponse.message,snackPosition:SnackPosition.BOTTOM,backgroundColor: AppColor.primaryOrangeColor);
          return '';
        }
        return apiResponse.data.data.url;
  }
}