import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/screens/login_screen/login_screen.dart';
import 'package:my_feedback_pal/screens/onboarding_screen/model.dart';
import 'package:my_feedback_pal/widgets/button_wiget.dart';

class OnboardingScreen extends StatefulWidget {
  static const String routeName = "/onboardingScreen";
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  var _controller = PageController();
  int _activePage = 0;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      setState(() {
        _activePage = int.parse(_controller.page.round().toString());
        print(_activePage);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.whiteColor1,
      body: PageView.builder(
          controller: _controller,
          itemCount: 3,
          itemBuilder: (context, index) => Center(
                  child: Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      buildImage(index),
                      SizedBox(height: 50),
                      buildTextColumn(index),
                      SizedBox(height: 12),
                      buildDotRow(),
                      _activePage == 2 ? buildLoginButton() : Container()
                    ],
                  ),
                  bottomHalfCircle(),
                  buildCircleCollection()
                ],
              ))),
    );
  }

  Padding buildLoginButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      child: ButtonWiget(
        title: 'Login',
        onTap: (){
          Navigator.pushReplacementNamed(context, LoginScreen.routeName);
        },
      ),
    );
  }

  Positioned buildCircleCollection() {
    return Positioned(
      bottom: -2,
      right: -1,
      child: Column(
        children: [
          Row(
            children: [
              Opacity(
                opacity: 0.5,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.pinkColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.5,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.pinkColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.5,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.pinkColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Opacity(
                opacity: 0.5,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.pinkColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.5,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.pinkColor,
                    radius: 6,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.5,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    backgroundColor: AppColor.pinkColor,
                    radius: 6,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Positioned bottomHalfCircle() {
    return Positioned(
      left: -50,
      bottom: -60,
      child: Opacity(
        opacity: 0.2,
        child: Container(
          height: 150,
          width: 150,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(75),
              border:
                  Border.all(width: 25, color: AppColor.primaryOrangeColor)),
        ),
      ),
    );
  }

  Padding buildImage(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
      child: Image.asset(onBoardingModelData[index].image),
    );
  }

  Row buildDotRow() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      Opacity(
        opacity: _activePage == 0 ? 1 : 0.6,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: CircleAvatar(
            backgroundColor: AppColor.dotActiveColor,
            radius: 4,
          ),
        ),
      ),
      Opacity(
        opacity: _activePage == 1 ? 1 : 0.6,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: CircleAvatar(
            backgroundColor: AppColor.dotActiveColor,
            radius: 4,
          ),
        ),
      ),
      Opacity(
        opacity: _activePage == 2 ? 1 : 0.6,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: CircleAvatar(
            backgroundColor: AppColor.dotActiveColor,
            radius: 4,
          ),
        ),
      ),
    ]);
  }

  Padding buildTextColumn(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Text(
            onBoardingModelData[index].title,
            style: TextStyle(
                color: AppColor.blackColor,
                fontSize: 20,
                fontWeight: FontWeight.w900),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 12),
          Text(
            onBoardingModelData[index].description,
            style: TextStyle(
                color: AppColor.blackColor,
                fontSize: 14,
                fontWeight: FontWeight.w400),
            textAlign: TextAlign.center,
            maxLines: 2,
          ),
        ],
      ),
    );
  }
}
