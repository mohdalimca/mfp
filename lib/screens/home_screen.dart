import 'package:flutter/material.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:intl/intl.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/screens/home_tab/home_tab.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/homeScreen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

final tabOptions = [
  HomeTab(),
  Text(''),
  Text(''),
];

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;
  @override
  void initState() {
    super.initState();
  }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabOptions[selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(icon: Icon(Icons.chat), title: Text('Chat')),
          BottomNavigationBarItem(
              icon: Icon(Icons.notifications), title: Text('Notifications')),
        ],
        currentIndex: selectedIndex,
        fixedColor: AppColor.primaryOrangeColor,
        onTap: onItemTapped,
      ),
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
}
