import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:intl/intl.dart';
import 'package:my_feedback_pal/screens/home_tab/HomeMananger.dart';
import 'package:my_feedback_pal/screens/profile_screen/profile_screen_manager.dart';
import 'package:my_feedback_pal/screens/team_member_profile_screen/team_member_profile_screen.dart';
import 'package:my_feedback_pal/widgets/profile_image_widget.dart';

import '../profile_screen/profile_screen.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  ProfileScreenManager _profileScreenManager = Get.put(ProfileScreenManager());
  bool _isLoading = false;
  HomeManager _homeManager = Get.put(HomeManager());

  @override
  void initState() {
    super.initState();
    getUserInfo();
    _homeManager.callManagerHomeApi();
  }

  @override
  void dispose() {
    super.dispose();
    _homeManager.onClose();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? Center(child: CupertinoActivityIndicator())
        : buildBody(context);
  }

  SafeArea buildBody(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      child: Column(
        children: [buildTopRow(context), buildTeamMemberList()],
      ),
    ));
  }

  Padding buildTeamMemberList() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  'Your team',
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  '03 out of 07 reviews done',
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
              ],
            ),
            SizedBox(height: 20),
            Obx(
              () => Container(
                height: 230,
                child: ListView.builder(
                    itemCount: _homeManager.empList.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            width: 130,
                            decoration: BoxDecoration(
                                color: AppColor.whiteColor,
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: [
                                  BoxShadow(
                                      color: AppColor.blackColor54,
                                      spreadRadius: 0.5,
                                      blurRadius: 5)
                                ]),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 110,
                                    decoration: BoxDecoration(
                                        color: AppColor.primaryOrangeColor,
                                        borderRadius: BorderRadius.circular(8),
                                        image: DecorationImage(
                                            image: CachedNetworkImageProvider(
                                                _homeManager.empList[index]
                                                        .userPic ??
                                                    ''),
                                            fit: BoxFit.cover),
                                        boxShadow: [
                                          BoxShadow(
                                              color: AppColor.blackColor54,
                                              spreadRadius: 0.5,
                                              blurRadius: 5)
                                        ]),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    _homeManager.empList[index].firstname ??
                                        '' +
                                            ' ' +
                                            _homeManager
                                                .empList[index].lastname ??
                                        '',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                FlatButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, TeamMemberProfileScreen.routeName);
                                  },
                                  child: Text(
                                    'Submit review',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w200),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  color: AppColor.grey200Color,
                                  shape: StadiumBorder(),
                                )
                              ],
                            ),
                          ),
                        )),
              ),
            )
          ]),
    );
  }

  Padding buildTopRow(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Good Morning',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: AppColor.blackColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(
                    DateFormat('MMM dd-yy').format(DateTime.now()),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: AppColor.blackColor,
                        fontSize: 27,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, ProfileScreen.routeName);
                  },
                  child: ProfileImageWidget(
                      url: _profileScreenManager.getUserProfileUrl(), size: 50))
            ],
          )
        ],
      ),
    );
  }

  getUserInfo() async {
    setState(() {
      _isLoading = true;
    });
    await _profileScreenManager.getUser();
    setState(() {
      _isLoading = false;
    });
  }
}
