
class ManagerHomeResponseModel {
    ManagerHomeResponseModel({
        this.version,
        this.status,
        this.errorcode,
        this.message,
        this.data,
    });

    Version version;
    int status;
    int errorcode;
    String message;
    List<ManagerHome> data;

    factory ManagerHomeResponseModel.fromJson(Map<String, dynamic> json) => ManagerHomeResponseModel(
        version: json["version"] == null ? null : Version.fromJson(json["version"]),
        status: json["status"] == null ? null : json["status"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<ManagerHome>.from(json["data"].map((x) => ManagerHome.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "version": version == null ? null : version.toJson(),
        "status": status == null ? null : status,
        "errorcode": errorcode == null ? null : errorcode,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class ManagerHome {
    ManagerHome({
        this.totalEmp,
        this.empInfo,
    });

    String totalEmp;
    List<EmpInfo> empInfo;

    factory ManagerHome.fromJson(Map<String, dynamic> json) => ManagerHome(
        totalEmp: json["total_emp"] == null ? null : json["total_emp"],
        empInfo: json["emp_info"] == null ? null : List<EmpInfo>.from(json["emp_info"].map((x) => EmpInfo.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total_emp": totalEmp == null ? null : totalEmp,
        "emp_info": empInfo == null ? null : List<dynamic>.from(empInfo.map((x) => x.toJson())),
    };
}

class EmpInfo {
    EmpInfo({
        this.id,
        this.firstname,
        this.lastname,
        this.designation,
        this.userEmail,
        this.userPic,
    });

    String id;
    String firstname;
    String lastname;
    String designation;
    String userEmail;
    String userPic;

    factory EmpInfo.fromJson(Map<String, dynamic> json) => EmpInfo(
        id: json["id"] == null ? null : json["id"],
        firstname: json["firstname"] == null ? null : json["firstname"],
        lastname: json["lastname"] == null ? null : json["lastname"],
        designation: json["designation"] == null ? null : json["designation"],
        userEmail: json["user_email"] == null ? null : json["user_email"],
        userPic: json["user_pic"] == null ? null : json["user_pic"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "firstname": firstname == null ? null : firstname,
        "lastname": lastname == null ? null : lastname,
        "designation": designation == null ? null : designation,
        "user_email": userEmail == null ? null : userEmail,
        "user_pic": userPic == null ? null : userPic,
    };
}

class Version {
    Version({
        this.status,
        this.versioncode,
        this.versionmessage,
        this.currentVersion,
    });

    int status;
    int versioncode;
    String versionmessage;
    String currentVersion;

    factory Version.fromJson(Map<String, dynamic> json) => Version(
        status: json["status"] == null ? null : json["status"],
        versioncode: json["versioncode"] == null ? null : json["versioncode"],
        versionmessage: json["versionmessage"] == null ? null : json["versionmessage"],
        currentVersion: json["current_version"] == null ? null : json["current_version"],
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "versioncode": versioncode == null ? null : versioncode,
        "versionmessage": versionmessage == null ? null : versionmessage,
        "current_version": currentVersion == null ? null : currentVersion,
    };
}
