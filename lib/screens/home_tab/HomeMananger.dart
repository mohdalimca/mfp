import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:my_feedback_pal/constants/api_constant.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/constants/string_constant.dart';
import 'package:my_feedback_pal/screens/home_tab/model.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';
import 'package:my_feedback_pal/utils/k3webservice.dart';
import 'package:my_feedback_pal/widgets/loading_indication.dart';

class HomeManager extends GetxController {
  var empList = List<EmpInfo>().obs;

  @override
  void onInit() {
    super.onInit();
  }

  callManagerHomeApi() async {
    await Future.delayed(
        Duration.zero);
    showLoadingIndicator(Get.context);
    User user = await AppUtils.getUser();
    var headers = {'Authorization': user.authorization};
    ApiResponse<ManagerHomeResponseModel> apiResponse =
        await K3Webservice.getMethod(Apis.homeManager, headers);
    hideLoadingIndicator(Get.context);
    if (apiResponse.error) {
      Get.snackbar(StringConstant.appName, apiResponse.message,
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: AppColor.primaryOrangeColor,
          colorText: AppColor.whiteColor);
      return;
    }
    empList.value = apiResponse.data.data.first.empInfo;
  }

  @override
  void onClose() {
    super.onClose();
  }
}
