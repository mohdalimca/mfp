import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/screens/home_screen.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';
import 'package:my_feedback_pal/screens/onboarding_screen/onboarding_screen.dart';

import 'login_screen/login_screen.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = "/";
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: Get.height,
        width: Get.width,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/images/Image45@2x.png'),
          fit: BoxFit.cover,
        )),
        child: Stack(
          children: [
            Container(
              height: Get.height,
              width: Get.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: <Color>[Color(0xCCF35B2D), Color(0x80FAA33C)],
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/logo@2x.png',
                      height: 100,
                    ),
                    SizedBox(height: 20),
                    Text(
                      'MY FEEDBACK PAL!',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 21,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    )
                  ]),
            ),
          ],
        ),
      ),
    );
  }

  void checkLogin() async {
    await Future.delayed(Duration(seconds: 3));
    User user = await AppUtils.getUser();
    if (user == null)
      Navigator.pushReplacementNamed(context, OnboardingScreen.routeName);
    else
      Navigator.pushNamed(context, HomeScreen.routeName);
  }
}
