import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_feedback_pal/constants/api_constant.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/constants/app_utils.dart';
import 'package:my_feedback_pal/constants/string_constant.dart';
import 'package:my_feedback_pal/screens/login_screen/model.dart';
import 'package:my_feedback_pal/utils/k3webservice.dart';
import 'package:my_feedback_pal/widgets/loading_indication.dart';

class ChangePasswordManager {
  callChangePasswordApi(
      BuildContext context, Map<String, dynamic> values) async {
    showLoadingIndicator(context);
    User user = await AppUtils.getUser();
    var headers = {'Authorization': user.authorization,'user_id':user.userId};
    ApiResponse<CommonResponseModel> apiResponse =
        await K3Webservice.postMethod(Apis.changePassword, values, headers);
    hideLoadingIndicator(context);
    if (apiResponse.error) {
      Get.snackbar(StringConstant.appName, apiResponse.message,
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: AppColor.primaryOrangeColor,
          colorText: AppColor.whiteColor);
      return;
    }
    Get.snackbar(StringConstant.appName, apiResponse.data.message,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: AppColor.primaryOrangeColor,
        colorText: AppColor.whiteColor);
    await Future.delayed(Duration(seconds: 3));
    Navigator.pop(context);
  }
}
