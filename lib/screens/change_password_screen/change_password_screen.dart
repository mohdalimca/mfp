import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_feedback_pal/constants/app_colors.dart';
import 'package:my_feedback_pal/screens/change_password_screen/change_password_manager.dart';
import 'package:my_feedback_pal/widgets/back_button_widget.dart';
import 'package:my_feedback_pal/widgets/button_wiget.dart';

class ChangePasswordScreen extends StatefulWidget {
  static const String routeName = '/changePasswordScreen';
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  FocusNode oldpasswordFocusNode = FocusNode();
  FocusNode newpasswordFocusNode = FocusNode();
  FocusNode confirmpasswordFocusNode = FocusNode();

  ChangePasswordManager _changePasswordManager = ChangePasswordManager();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          buildAppBar(context),
                          SizedBox(height: 50),
                          buildForm(),
                          SizedBox(height: 50),
                          ButtonWiget(
                              onTap: () {
                                if (_fbKey.currentState.saveAndValidate()) {
                                  print(_fbKey.currentState.value);
                                  FocusScope.of(context).unfocus();
                                  _changePasswordManager.callChangePasswordApi(context, _fbKey.currentState.value);
                                }
                              },
                              title: 'Change Password')
                        ])))));
  }

  Row buildAppBar(BuildContext context) {
    return Row(
      children: [
        BackButtonWidget(onTap: () {
          Navigator.pop(context);
        }),
        Text(
          'Change Password',
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
        )
      ],
    );
  }

  Widget buildForm() {
    return Column(children: [
      FormBuilder(
        key: _fbKey,
        child: Column(children: [
          FormBuilderTextField(
            attribute: "old_password",
            focusNode: oldpasswordFocusNode,
            decoration: InputDecoration(
                labelText: "Old Password",
                labelStyle: TextStyle(color: AppColor.blackColor)),
            obscureText: true,
            maxLines: 1,
            onFieldSubmitted: (text) {
              newpasswordFocusNode.requestFocus();
            },
            validators: [
              FormBuilderValidators.required(),
            ],
          ),
          SizedBox(height: 10),
          FormBuilderTextField(
            attribute: "new_password",
            focusNode: newpasswordFocusNode,
            decoration: InputDecoration(
                labelText: "New Password",
                labelStyle: TextStyle(color: AppColor.blackColor)),
            obscureText: true,
            maxLines: 1,
            onFieldSubmitted: (text) {
              newpasswordFocusNode.unfocus();
            },
            validators: [
              FormBuilderValidators.required(),
            ],
          ),
        ]),
      )
    ]);
  }
}
